vim.opt.textwidth = 0
vim.opt.wrapmargin = 0
vim.opt.wrap = true
vim.opt.linebreak = true
--vim.cmd(':TableModeEnable')
-- vim.cmd(':TSBufDisable highlight')
vim.opt.conceallevel = 2
